import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Car } from '../../models/car';

@Component({
  // any valid css selector
  selector: 'tr.car-row',
  templateUrl: './car-table-row.component.html',
  styleUrls: ['./car-table-row.component.css']
})
export class CarTableRowComponent implements OnInit {

  @Input()
  car: Car = null;

  @Output()
  edit = new EventEmitter<number>();

  @Output()
  delete = new EventEmitter<number>();


  constructor() { }

  ngOnInit() {
  }

  doEdit() {
    this.edit.emit(this.car.id);
  }

  doDelete() {
    this.delete.emit(this.car.id);
  }

}
