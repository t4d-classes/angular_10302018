import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Car } from '../../models/car';

@Component({
  selector: 'tr.car-edit-row',
  templateUrl: './car-table-edit-row.component.html',
  styleUrls: ['./car-table-edit-row.component.css']
})
export class CarTableEditRowComponent implements OnInit {

  @Input() car: Car = null;
  @Output() save = new EventEmitter<Car>();
  @Output() cancel = new EventEmitter<null>();

  carEditForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.carEditForm = this.fb.group({
      editMake: [ this.car.make ],
      editModel: [ this.car.model ],
      editYear: [ this.car.year ],
      editColor: [ this.car.color ],
      editPrice: [ this.car.price ],
    });
  }

  doSave() {
    this.save.emit({
      id: this.car.id,
      make: this.carEditForm.value.editMake,
      model: this.carEditForm.value.editModel,
      year: this.carEditForm.value.editYear,
      color: this.carEditForm.value.editColor,
      price: this.carEditForm.value.editPrice,
    });
  }

  doCancel() {
    this.cancel.emit();
  }

}
