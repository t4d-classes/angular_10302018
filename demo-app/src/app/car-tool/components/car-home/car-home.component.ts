import { Component, OnInit } from '@angular/core';

import { Car } from '../../models/car';
import { CarsService } from '../../services/cars.service';


@Component({
  selector: 'car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css'],
})
export class CarHomeComponent implements OnInit {

  headerText = 'Car Tool';

  public editCarId = -1;

  cars: Car[] = [];

  constructor(private carsSvc: CarsService) { }

  ngOnInit() {
    this.doRefresh();
  }

  doRefresh() {
    this.carsSvc.all().subscribe(cars => this.cars = cars);
  }

  doAddCar(car: Car) {
    this.editCarId = -1;
    this.carsSvc.append(car).then(() => this.doRefresh());
  }

  doEditCar(carId: number) {
    this.editCarId = carId;
  }

  doDeleteCar(carId: number) {
    this.carsSvc.delete(carId);
    this.editCarId = -1;
  }

  doSaveCar(car: Car) {
    this.carsSvc.replace(car);
    this.editCarId = -1;
  }

  doCancelCar() {
    this.editCarId = -1;
  }

}
