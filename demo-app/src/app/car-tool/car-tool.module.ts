import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { SharedModule } from '../shared/shared.module';

import { CarsService } from './services/cars.service';

import { CarHomeComponent } from './components/car-home/car-home.component';
import { CarTableComponent } from './components/car-table/car-table.component';
import { CarTableRowComponent } from './components/car-table-row/car-table-row.component';
import { CarFormComponent } from './components/car-form/car-form.component';
import { CarTableEditRowComponent } from './components/car-table-edit-row/car-table-edit-row.component';

@NgModule({
  imports: [
    CommonModule, SharedModule, ReactiveFormsModule, HttpClientModule,
  ],
  declarations: [
    CarHomeComponent, CarTableComponent,
    CarTableRowComponent, CarFormComponent, CarTableEditRowComponent,
  ],
  exports: [CarHomeComponent],
  providers: [ CarsService ],
})
export class CarToolModule { }
