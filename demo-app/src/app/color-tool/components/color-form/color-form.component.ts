import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';


// const myRequired = (c: AbstractControl) => {

//   if (!c) {
//     return {
//       myRequired: true,
//     };
//   }

//   if (c.value == null || String(c.value).length === 0) {
//     return {
//       myRequired: true,
//       message: 'Nate the Great!',
//     };
//   }

//   return null;
// };

const eitherControl = (controlNames: string[]) => {

  return (fg: FormGroup) => {

    console.log('called validator');

    for (let x = 0; x < controlNames.length; x++) {
      if (fg.controls[controlNames[x]].value != null
          && String(fg.controls[controlNames[x]].value).length !== 0 ) {
        return null;
      }
    }

    console.log('made it here');

    return {
      eitherControl: true,
      controlNames,
    };

  };
};

@Component({
  selector: 'color-form',
  templateUrl: './color-form.component.html',
  styleUrls: ['./color-form.component.css']
})
export class ColorFormComponent implements OnInit {

  colorForm: FormGroup;

  @Output()
  submitColor = new EventEmitter<string>();

  @Input()
  submitButtonText = 'Submit Color';

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.colorForm = this.fb.group({
      'colorName': [ '', { validators: [ Validators.minLength(3) ] } ],
      'colorHexCode': [ '', { validators: [ Validators.minLength(3) ] } ],
    }, { validator: [ eitherControl(['colorName', 'colorHexCode']) ] });
  }

  doSubmitColor() {
    this.submitColor.emit(this.colorForm.value.color);
    this.colorForm.reset();
    // this.colorForm.controls.color.setValue('');
  }

}
