import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myUppercase',
  pure: false,
})
export class MyUppercasePipe implements PipeTransform {

  transform(value: any): any {
    console.log('my uppercase transform');
    const sValue = String(value);
    return sValue.toUpperCase();
  }

}
