# Welcome to Angular Class!

## Instructor

Eric Greene - [http://t4d.io](http://t4d.io)

## Schedule

Class:

- Tuesday through Thursday: 8:30am to 4:30pm

Breaks:

- Morning Break: 10:15am to 10:30am
- Lunch: 12pm to 1pm
- Afternoon Break: 2:45pm to 3pm

## Course Outline

- Day 1 - Angular CLI, Overview of Angular, Modules, Components, Composition
- Day 2 - Pipes, Reactive Forms, Services
- Day 3 - HttpClient & REST Services, RxJS, Routing, Unit Testing

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Microsoft Virtual Academy](https://mva.microsoft.com/search/SearchResults.aspx#!q=Eric%20Greene&lang=1033)
- [Angular Tutorial](https://github.com/Microsoft/TechnicalCommunityContent/tree/master/Web%20Frameworks/Angular/Session%202%20-%20Hands%20On)

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)

## Useful Resources

- [Angular CLI](https://cli.angular.io/)
- [TypeScript Coding Guidelines](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines)
- [Angular Style Guide](https://angular.io/docs/ts/latest/guide/style-guide.html)
- [Angular Cheat Sheet](https://angular.io/docs/ts/latest/guide/cheatsheet.html)
- [Angular API](https://angular.io/docs/ts/latest/api/)
